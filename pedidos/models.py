from django.db import models

# Create your models here.

class Pedido(models.Model):

    cliente = models.CharField(max_length=100)
    productos  = models.JSONField()
    estado = models.CharField( max_length=50)
    entrada = models.DateTimeField(auto_now_add=True)
    salida = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    tiempo_total = models.TimeField(auto_now=False, auto_now_add=False, null=True)
    total = models.IntegerField()

