from django.shortcuts import render
from datetime import datetime, timezone
import json
from . import models
from . import serializers
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import status # Posibly enum class with most of the http response with description so it is easier to find
from rest_framework.views import APIView
from productos.models import Producto
from productos.serializers import ProductoSerializer
from cafeteriarest.permisos import IsCocinero, IsRecepcionista, IsRecepcionistaOrCocinero

class PedidosApiView(APIView):
    def get(self, request):
        pedidos = models.Pedido.objects.all()
        
        # orderby; orderdirection; filterby; filtervalue
        # TODO: ordenar y filtrar por listos para servir para el recepcionista, y 
        # cocina para el cocinero
        # ordenar_por = request.GET.get('orderby', '')
        # ascOrDesc = request.GET.get('orderdirection', '')
        # campoFiltro = request.GET.get('filterby', '')
        # filtroValue = request.GET.get('filtervalue', '')
        print("IS COCINERO", IsCocinero.has_permission(self, request, None), "[IS COCINERO]", [IsCocinero.has_permission(self, request, None)])
        if IsRecepcionista.has_permission(self, request, None):
            pedidos = pedidos.filter(estado='servir').order_by('-salida')
        elif IsCocinero.has_permission(self, request, None):
            pedidos = pedidos.filter(estado='cocina').order_by('-entrada')
        else:
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)

        respuesta = serializers.PedidoSerializer(pedidos, many=True)
        return Response(respuesta.data, status=status.HTTP_200_OK)

    def post(self, request):
        print("ENTRO EN EL POST")
        if IsRecepcionista.has_permission(self, request, None):
            if (request.data.get('total') is None or request.data.get('total') == 0):
                total = 0
                try:
                    print(request.data['productos'])
                    productos = []
                    #productos.append(json.loads(str(request.data['productos'])))
                    print(productos)
                    for producto in request.data['productos']:
                        print(producto["id"])
                        total = total + (Producto.objects.get(pk=producto["id"]).precio * producto["cantidad"])
                except Exception as e:
                    print(e)
                    return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                print(total)
                request.data['total'] = total
            serialized_data = serializers.PedidoSerializer(data=request.data)
            if serialized_data.is_valid():
                serialized_data.save()
                return Response(serialized_data.data, status=status.HTTP_201_CREATED)

            return Response(serialized_data.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)

class PedidoApiView(APIView):
    
    def get(self, request, id):     
        if IsRecepcionistaOrCocinero.has_permission(self, request, None):
            try:
                pedido = models.Pedido.objects.get(pk=id)
                productos = pedido.productos
                lista_productos = []
                for producto in productos:
                    lista_productos.append(Producto.objects.get(pk=producto))
                return Response(ProductoSerializer(lista_productos, many=True).data, 
                                status=status.HTTP_302_FOUND)
            except Exception as e:
                print(e)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else: 
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)

    def put(self, request, id):

        try:
            pedido = models.Pedido.objects.get(pk=id)
            print(request)
            if (request.data['estado']  == 'servir' and IsCocinero.has_permission(self, request, None)):
                #time_finish = datetime.now(timezone(offset))
                #time_start = datetime.strptime(request.data['entrada'], "%Y-%m-%dT%H:%M:%S.%fZ")
                #pedido.salida = str(time_finish)
                time_start = datetime.strptime(request.data['entrada'], "%Y-%m-%dT%H:%M:%S.%fZ")
                time_finish = datetime.strptime(request.data['salida'], "%Y-%m-%dT%H:%M:%S.%fZ")
                pedido.salida = str(time_finish)
                time_total = time_finish - time_start
                try:
                    time_total_obj = datetime.strptime(str(time_total), "%H:%M:%S.%f")
                except Exception as e:
                    print("la excepcion fue",e)
                    time_total_obj = datetime.strptime(str(time_total), "%d day, %H:%M:%S.%f")
                formatted_time_total = time_total_obj.strftime("%H:%M:%S.%f")
                print("El tiempo total ", formatted_time_total)
                pedido.tiempo_total = str(formatted_time_total)
                pedido.estado = "servir"
                #pedido.tiempo_total = time_total
                #pedido.productos = request.data['productos']
            else:
                return Response("No tiene autorizacion para indicar si un pedido esta listo para servirse", status=status.HTTP_403_FORBIDDEN)
            # serialized_data = serializers.PedidoSerializer(pedido, data=request.data)
            # print(serialized_data.data)
            pedido.save()
            return Response("Pedido Guardado", status=status.HTTP_200_OK)
        except Exception as e:
            print(" el error fue", e)
            return Response(e, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def delete(self, request, id):
        #pedido.delete()      
        if IsRecepcionista.has_permission(self, request, None):
            try:
                pedido = models.Pedido.objects.get(pk=id)
                pedido.estado = 'entregado'
                pedido.save()
                return Response(None, status=status.HTTP_200_OK) 
            except Exception as e:
                print(e)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else: 
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)

    