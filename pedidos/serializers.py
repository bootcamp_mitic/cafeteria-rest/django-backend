from rest_framework import serializers
from . import models
import json


class PedidoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Pedido
        fields = ['id', 'cliente', 'productos', 'total', 'estado', 'entrada', 'salida', 'tiempo_total']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        #representation['productos'] = json.loads(representation['productos'])
        representation['productos'] = json.dumps(representation['productos'])
        return representation