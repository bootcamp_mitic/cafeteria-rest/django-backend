from django.urls import path, include
from . import views
urlpatterns = [
    #path('peliculas', views.peliculas), #Django Native or w/o ApiView Class
    #path('peliculas/<int:id>', views.pelicula),#Django Native or w/o ApiView Class
    path('pedidos/', views.PedidosApiView.as_view()),# with ApiView class
    path('pedidos/<int:id>', views.PedidoApiView.as_view()),#Django Native or w/o ApiView Class
]
