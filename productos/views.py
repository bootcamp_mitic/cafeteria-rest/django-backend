from django.shortcuts import render
from rest_framework import viewsets
from . import models, serializers
from cafeteriarest.permisos import IsUser
# Create your views here.

class ProductosViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProductoSerializer
    queryset = models.Producto.objects.all()
    permission_classes = [IsUser]