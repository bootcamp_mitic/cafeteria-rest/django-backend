from rest_framework import serializers
from . import models


class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Producto
        fields = ['id', 'nombre', 'precio', 'cantidad']