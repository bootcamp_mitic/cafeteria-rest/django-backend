from rest_framework import viewsets
from django.contrib.auth.models import User, Group
from rest_framework.views import APIView
from .serializers import UserSerializer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import status
from cafeteriarest.permisos import IsAdmin, IsUser
'''
class UsersViewSet(viewsets.ModelViewSet):
    # Minimamente hay que pasar queryset y serializer_class
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsUser]

'''
class UsuariosApiView(APIView):

    def get(self, request):
        if (IsUser.has_permission(self, request, None)):
            users = User.objects.all()
            userResponse = UserSerializer(users, many=True)
            return Response(userResponse.data,status=status.HTTP_200_OK)

        return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)

    def post(self, request):
        if IsAdmin.has_permission(self, request, None):
            try:                
                create_user_with_group(request)
                return Response(None, status=status.HTTP_201_CREATED)
            except Exception as e:
                print("Error en POST usuarios", e)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)

class UsuarioApiView(APIView):

    def get(self, request, id):     
        if IsUser.has_permission(self, request, None):
            try:
                user = User.objects.get(pk=id)
                return Response(UserSerializer(user).data, 
                                    status=status.HTTP_302_FOUND)
            except Exception as err:
                print ("error al retornar un usuario", err)
                return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)
            
    def put(self, request, id):
        if IsAdmin.has_permission(self, request, None):
            update_user_with_group(request=request, id=id)
            return Response("Usuario Actualizado", status=status.HTTP_200_OK)

    def delete(self, request, id):
        if IsAdmin.has_permission(self, request, None):
            try:
                user = User.objects.get(pk=id)
                user.delete()
                return Response(None, status=status.HTTP_200_OK) 
            except Exception as err:
                print("Error al eliminar", err)
                return Response("Error al eliminar", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response("No tiene autorizacion para utilizar este recurso", status=status.HTTP_403_FORBIDDEN)
    

def create_user_with_group(request):
    print("POST REQUEST ", request.data)
    username = request.data.get('username')
    password = request.data.get('password')
    group_name = request.data.get('group_name')
    print("antes de crear el usuario")
    user = User.objects.create_user(username=username, password=password)
    print("creo el usuario")
    try:
        group = Group.objects.get(name=group_name)
    except Group.DoesNotExist as err:
        print( err )
        return Response("Grupo no Existe", status=status.HTTP_400_BAD_REQUEST)

    user.groups.add(group)
    user.save()

def update_user_with_group(request, id):
    name = request.data.get('username')
    password = request.data.get('password')
    first_name = request.data.get('first_name')
    last_name = request.data.get('last_name')
    group_name = request.data.get('group_name')
    try:
        group = Group.objects.get(name=group_name)
        user = User.objects.get(pk = id)
    except User.DoesNotExist as err:
        print (err)
        return Response("Usuario Inexistente", status=status.HTTP_400_BAD_REQUEST)
    except Group.DoesNotExist as err:
        print( err )
        return Response("Grupo no Existe", status=status.HTTP_400_BAD_REQUEST)
    user.first_name = first_name
    user.last_name = last_name
    user.set_password(password)
    user.username = name
    user.groups.clear()
    user.groups.add(group)
    user.save()
        
