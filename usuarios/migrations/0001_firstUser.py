from django.db import migrations
from django.contrib.auth.models import User, Group

def insert_data(apps, schema_editor):
    # Create a new user
    user = User.objects.create_user(username='admin', password='admin')

    # Create a new user group
    group = Group.objects.create(name='admin')
    group1 = Group.objects.create(name='recepcion')
    group2 = Group.objects.create(name='cocina')

    # Add the user to the group
    user.groups.add(group)

class Migration(migrations.Migration):

    dependencies = [
        # Specify any dependencies if required
    ]

    operations = [
        migrations.RunPython(insert_data),
    ]
