'''
from rest_framework import routers
from .views import UsersViewSet

router = routers.DefaultRouter()
router.register(r'usuarios', UsersViewSet, basename='users')


urlpatterns = router.urls


'''
from django.urls import path, include
from . import views

urlpatterns = [
    path('usuarios/', views.UsuariosApiView.as_view()),# with ApiView class
    path('usuarios/<int:id>', views.UsuarioApiView.as_view()),#Django Native or w/o ApiView Class
]
